//
//  ViewController.swift
//  APIDemo
//
//  Created by Kapil Dhawan on 19/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

struct Course: Codable {
    var id = Int()
    var name = String()
    var link = String()
    var imageUrl = String()
    var number_of_lessons = Int()
    
}

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonURL = "https://api.letsbuildthatapp.com/jsondecodable/course"
        guard let url = URL(string: jsonURL) else { return }
        
        URLSession.shared.dataTask(with: url) {  (data,response,error) in
            guard let data = data else { return }
            
            do{
                let course = try JSONDecoder().decode(Course.self, from: data)
                
                print(course.name)
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }
            }.resume()
    }
    
}



